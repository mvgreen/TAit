package com.mvgreen.tait

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mvgreen.tait.DbViewModel.Companion.ActivityMode.ACTIVITY_DATABASE
import com.mvgreen.tait.R.id.*
import kotlinx.android.synthetic.main.activity_database.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.*
import com.mvgreen.tait.data.Repository
import com.mvgreen.tait.data.db.entities.Category
import com.mvgreen.tait.data.db.entities.Record
import kotlinx.android.synthetic.main.db_fragment.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import pub.devrel.easypermissions.AppSettingsDialog

class MainListActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    enum class Action {
        ADD_CATEGORY, MERGE_CATEGORIES, IMPORT_RECORDS
    }

    val vm: DbViewModel by lazy { ViewModelProviders.of(this).get(DbViewModel::class.java) }

    companion object {
        private const val SHARE_FILES = 1
        private const val IMPORT_FILES = 2
        private const val SHARE_SINGLE_FILE = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_database)
        // Настройка верхнего аппбара
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(
            if (vm.activityMode == ACTIVITY_DATABASE)
                R.menu.db_opts
            else
                R.menu.search_opts,
            menu
        )
        return true
    }

    override fun onResume() {
        super.onResume()

        if (vm.activityMode == ACTIVITY_DATABASE) {
            supportActionBar!!.setTitle(R.string.db_mode_title)
            fab.setImageResource(R.drawable.ic_add)
        } else {
            supportActionBar!!.setTitle(R.string.search_mode_title)
            fab.setImageResource(R.drawable.baseline_photo_camera_24)
        }
        supportActionBar!!.setTitle(
            if (vm.activityMode == ACTIVITY_DATABASE) R.string.db_mode_title
            else R.string.search_mode_title
        )
        // Настройка нижнего аппбара
        with(bottom_appbar) {
            replaceMenu(R.menu.activity_mainlist_bottom)
            setOnMenuItemClickListener { onOptionsItemSelected(it) }
            setNavigationOnClickListener {
                BottomNavigationDrawerFragment().apply { show(supportFragmentManager, tag) }
            }
            menu.findItem(search)
                .setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                        this@MainListActivity.findViewById<FloatingActionButton>(R.id.fab).hide()
                        vm.searchOpened = true
                        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManager.toggleSoftInputFromWindow(
                            applicationWindowToken,
                            InputMethodManager.SHOW_FORCED, 0
                        )

                        return true
                    }

                    override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
                        this@MainListActivity.findViewById<FloatingActionButton>(R.id.fab).show()
                        vm.searchOpened = false
                        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManager.hideSoftInputFromWindow(applicationWindowToken, 0)

                        return true
                    }
                }).also {
                    if (vm.searchOpened)
                        it.expandActionView()
                    else
                        it.collapseActionView()
                }
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onBackPressed() {
        if (vm.searchOpened)
            bottom_appbar.menu.findItem(search).collapseActionView()
        else
            super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            refresh -> {
                vm.clear()
                recreate()
            }
            add_cat -> showCategoryNameDialog(Action.ADD_CATEGORY)
            merge_cats -> mergeCategories()
            share -> shareCategory()
            import_file -> importFile()
            delete_cat -> showDeleteDialog()
        }

        return true
    }

    fun showDeleteDialog(record: Record? = null) {
        AlertDialog.Builder(this)
            .setTitle(R.string.delete)
            .setMessage(if (record == null) R.string.delete_msg else R.string.delete_record_msg)
            .setPositiveButton(R.string.ok) { dialog, id ->
                showLoadingMessage()
                GlobalScope.launch {
                    if (record != null)
                        Repository.deleteRecord(record)
                    else {
                        Repository.bgDeleteCategory(
                            Category((view_pager.adapter as SectionsPagerAdapter).getPageTitle(category_tabs.selectedTabPosition)!!)
                        )
                    }
                    hideLoadingMessage()
                }
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    private fun importFile() {
        requestFilesAndPerform(IMPORT_FILES)
    }

    private fun showCategoryNameDialog(
        afterNameChoosed: Action,
        catListToReplace: MutableList<String>? = null, recordList: List<Record>? = null
    ) {
        val view = layoutInflater.inflate(R.layout.add_category_dial, null)
        val dialog = AlertDialog.Builder(this)
            .setTitle(R.string.add_category)
            .setView(view)
            .setPositiveButton(R.string.ok, null)
            .setNegativeButton(R.string.cancel) { di, i -> }
            .create()

        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                view.findViewById<TextView>(name_incorrect).visibility = View.GONE
                view.findViewById<TextView>(name_occupied).visibility = View.GONE

                val categoryName = view.findViewById<EditText>(category_name).text.toString()
                if (!Repository.isValidCategoryName(categoryName)) {
                    view.findViewById<TextView>(name_incorrect).visibility = View.VISIBLE
                    return@setOnClickListener
                }

                val hasCategory = Repository.checkHasCategory(categoryName)
                val observer = object : Observer<Boolean> {
                    override fun onChanged(has: Boolean) {
                        when (afterNameChoosed) {
                            Action.ADD_CATEGORY -> {
                                if (has)
                                    view.findViewById<TextView>(name_occupied).visibility = View.VISIBLE
                                else {
                                    GlobalScope.launch {
                                        Repository.bgAddCategory(Category(categoryName))
                                    }
                                    dialog.dismiss()
                                }
                            }
                            Action.MERGE_CATEGORIES -> {
                                if (has && categoryName !in catListToReplace!!)
                                    view.findViewById<TextView>(name_occupied).visibility = View.VISIBLE
                                else {
                                    Repository.mergeCategories(catListToReplace!!, categoryName)
                                    dialog.dismiss()
                                }
                            }
                            Action.IMPORT_RECORDS -> {
                                GlobalScope.launch {
                                    if (!has)
                                        Repository.bgAddCategory(Category(categoryName))
                                    for (r in recordList!!)
                                        Repository.bgAddRecord(r, Category(categoryName))
                                }
                                dialog.dismiss()
                            }
                        }
                        hasCategory.removeObserver(this)
                    }
                }
                hasCategory.observeForever(observer)
            }
        }
        dialog.show()
    }


    private fun mergeCategories() {
        val view = layoutInflater.inflate(R.layout.list_dialog, null)
        val listView = view.findViewById<ListView>(list)
        val items = Repository.categoryNames.value!!.toMutableList()
        listView.adapter = ArrayAdapter<String>(
            this, R.layout.check_list_item,
            items
        )

        AlertDialog.Builder(this)
            .setTitle(R.string.merge_categories)
            .setView(view)
            .setPositiveButton(R.string.ok) { _, _ ->
                val checkedItems = listView.checkedItemPositions
                items.filterIndexed { index: Int, s: String ->
                    checkedItems[index]
                }.also {
                    if (!it.isEmpty())
                        showCategoryNameDialog(Action.MERGE_CATEGORIES, catListToReplace = it as MutableList<String>)
                }
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    private fun shareCategory() {
        AlertDialog.Builder(this)
            .setTitle(R.string.share_records)
            .setMessage(R.string.share_warning)
            .setPositiveButton(R.string.ok) { _, _ ->
                showLoadingMessage()
                requestFilesAndPerform(SHARE_FILES)
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    fun shareRecord(record: Record) {
        showLoadingMessage()
        vm.recordToShare = record
        requestFilesAndPerform(SHARE_SINGLE_FILE)
    }

    fun showLoadingMessage() {
        val view = layoutInflater.inflate(R.layout.loading_dial, null)
        val dialog = AlertDialog.Builder(this)
            .setTitle(R.string.share_records)
            .setView(view)
            .create()
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        hideLoadingMessage()
        vm.loadingDialog = dialog
    }

    fun hideLoadingMessage() {
        vm.loadingDialog?.dismiss()
        vm.loadingDialog = null
    }

    private fun requestFilesAndPerform(code: Int) {
        EasyPermissions.requestPermissions(
            PermissionRequest.Builder(this, code, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .setRationale(R.string.perm_requires_msg)
                .setNegativeButtonText(R.string.cancel)
                .setPositiveButtonText(R.string.ok)
                .build()
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this)
                .setRationale(R.string.perm_requires_msg)
                .setPositiveButton(R.string.ok)
                .setNegativeButton(R.string.cancel)
                .build()
                .show()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        when (requestCode) {
            SHARE_FILES -> {
                GlobalScope.launch {
                    if (!Repository.bgCreateCategoryFile(
                            this@MainListActivity,
                            view_pager.adapter?.getPageTitle(category_tabs.selectedTabPosition).toString()
                        )
                    ) {
                        showErrorDialog()
                    }
                    hideLoadingMessage()
                }
            }
            IMPORT_FILES -> Repository.importRecords(this@MainListActivity)
            SHARE_SINGLE_FILE -> {
                GlobalScope.launch {
                    if (!Repository.bgCreateRecordFile(this@MainListActivity))
                        showErrorDialog()
                    hideLoadingMessage()
                }
            }
        }
    }

    private fun showErrorDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.error_unknown)
            .setMessage(R.string.cant_save_file)
            .setNeutralButton(R.string.ok) { _, _ -> }
            .show()
    }

    fun onRecordImported(records: List<Record>) {
        showCategoryNameDialog(Action.IMPORT_RECORDS, recordList = records)
    }

    fun showRecordCategories(record: Record) {
        val view = layoutInflater.inflate(R.layout.list_dialog, null)
        val listView = view.findViewById<ListView>(list)
        val items = Repository.getRecordCategories(record)
        items.observe(this, Observer {
            listView.adapter = ArrayAdapter<String>(
                this@MainListActivity, R.layout.simple_list_item,
                it
            )

            AlertDialog.Builder(this@MainListActivity)
                .setTitle(R.string.show_categories)
                .setView(view)
                .setNeutralButton(R.string.close) { _, _ -> }
                .show()

        })
    }

}
