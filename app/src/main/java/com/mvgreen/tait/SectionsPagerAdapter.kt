package com.mvgreen.tait

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.mvgreen.tait.data.Repository

class SectionsPagerAdapter(fm: FragmentManager, val vm: DbViewModel) : FragmentStatePagerAdapter(fm) {

    private var titles = vm.tabTitles.value

    fun updateTitles(titles: List<String>) {
        this.titles = titles
        notifyDataSetChanged()
    }

    override fun getItem(position: Int) = TabFragment.newInstance(position, vm.activityMode == DbViewModel.Companion.ActivityMode.ACTIVITY_DATABASE)

    override fun getPageTitle(position: Int) = titles?.get(position)

    override fun getCount() = titles?.size ?: 0

    // Для полного перестроения представления при обновлении списка
    override fun getItemPosition(obj: Any) = PagerAdapter.POSITION_NONE
}

class TabFragment : Fragment() {

    val ARG_INDEX = "ARG_TABINDEX"
    val ARG_MODE = "ARG_MODE"

    lateinit var title: String
    lateinit var recyclerView: RecyclerView
    lateinit var viewPager: ViewPager

    companion object {
        fun newInstance(index: Int, isDbMode: Boolean) = TabFragment()
            .apply { arguments = Bundle().apply { putInt(ARG_INDEX, index); putBoolean(ARG_MODE, isDbMode) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_tabs, container, false)

        viewPager = container!! as ViewPager

        return rootView
    }

    override fun onResume() {
        super.onResume()

        val vpAdapter = viewPager.adapter as SectionsPagerAdapter

        if (vpAdapter.count != 0) {
            title = vpAdapter.getPageTitle(arguments!![ARG_INDEX] as Int) ?: "<MISSING TITLE>"
            val isDbMode = (arguments!![ARG_MODE] as Boolean)

            recyclerView = view!!.findViewById(R.id.cardList)
            with(recyclerView) {
                setHasFixedSize(false)
                layoutManager = LinearLayoutManager(context)
                adapter = DbListAdapter(context)
            }

            if (isDbMode) {
                Repository.getCategoryRecords(title).observe(this, Observer {
                    (recyclerView.adapter as DbListAdapter).update(it)
                })
            } else {
                Repository.findRecords(title).observe(this, Observer {
                    (recyclerView.adapter as DbListAdapter).update(it)
                })
            }
        }

    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        //result.restoreScroll();
    }

}