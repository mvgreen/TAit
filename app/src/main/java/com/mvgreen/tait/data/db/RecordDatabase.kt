package com.mvgreen.tait.data.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import android.content.Context
import com.mvgreen.tait.data.db.dao.CategoryDao
import com.mvgreen.tait.data.db.dao.RecordCategoryJoinDao
import com.mvgreen.tait.data.db.dao.RecordDao
import com.mvgreen.tait.data.db.entities.Category
import com.mvgreen.tait.data.db.entities.Record
import com.mvgreen.tait.data.db.entities.RecordCategoryJoin

@Database(entities = [Record::class, Category::class, RecordCategoryJoin::class], version = 1)
@TypeConverters(ListConverters::class)
abstract class RecordDatabase : RoomDatabase() {
    abstract fun recordDao(): RecordDao
    abstract fun categoryDao(): CategoryDao
    abstract fun joinDao(): RecordCategoryJoinDao

    /*class DBManager(private val dao: RecordDao) {

        private fun LiveData<Collection<Record>>.sort(query: String): List<Record> {
            // В первую очередь выводятся записи с точным совпадением заголовка,
            // затем с точным совпадением в тегах по их порядку к началу списка, остальные - в алфавитном порядке по заголовку
            return this.value?.sortedWith(Comparator { r1: Record, r2: Record ->
                if (query != "") {
                    // Проверка по совпадению с заголовком
                    if (r1.title == query)
                        return@Comparator -1
                    if (r2.title == query)
                        return@Comparator 1

                    // Позиция тега в списках
                    val tag1Pos = r1.tags.indexOf(query)
                    val tag2Pos = r2.tags.indexOf(query)

                    // Если тега нет ни в одном, или они находятся в равном приоритете, сравнение заголовков
                    if (tag1Pos == tag2Pos)
                        return@Comparator r1.title.compareTo(r2.title)

                    // Если тега нет в одном из списков
                    if (tag1Pos == -1)
                        return@Comparator 1
                    if (tag2Pos == -1)
                        return@Comparator -1

                    // Если есть, выше тот, у которого тег раньше
                    return@Comparator tag1Pos - tag2Pos
                }
                return@Comparator r1.title.compareTo(r2.title)
            })
        }

        fun findInCategories(query: String, vararg categories: String): List<Record> {
            val result = mutableSetOf<Record>()
            for (cat in categories) {
                result.addAll(dao.findInCategory(query, cat)
                        .filter { cat in it.categories }) // Чтобы избежать возможного совпадения в подстроках
            }
            return result.sort(query)
        }

        fun getAll() = dao.getAll().sort("")

        fun getById(id: Long) = dao.getById(id)

        fun insert(record: Record) = dao.insert(record)

        fun update(record: Record) = dao.update(record)

        fun delete(record: Record) = dao.delete(record)
    }*/

    companion object {
        private var DB: RecordDatabase? = null

        fun getDB(context: Context): RecordDatabase {
            if (DB == null) {
                synchronized(RecordDatabase::class) {
                    DB = Room.databaseBuilder(context.applicationContext, RecordDatabase::class.java, "record").build()
                }
            }
            return DB!!
        }

        fun destroyDataBase() {
            DB = null
        }
    }
}
