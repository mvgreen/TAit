package com.mvgreen.tait.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mvgreen.tait.data.db.entities.Record
import com.mvgreen.tait.data.db.entities.RecordCategoryJoin

@Dao
interface RecordCategoryJoinDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(entity: RecordCategoryJoin): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: RecordCategoryJoin)

    @Delete
    fun delete(entity: RecordCategoryJoin)

    @Query(
        "SELECT *FROM record_category_join WHERE categoryName=:categoryName"
    )
    fun getJoinsForCategory(categoryName: String): List<RecordCategoryJoin>

    @Query(
        "SELECT * FROM record INNER JOIN record_category_join ON " +
                "record.id=record_category_join.recordId WHERE record_category_join.categoryName=:categoryName"
    )
    fun getCategoryRecords(categoryName: String): LiveData<List<Record>>


    @Query(
        "SELECT * FROM record INNER JOIN record_category_join ON " +
                "record.id=record_category_join.recordId WHERE record_category_join.categoryName=:categoryName"
    )
    fun bgGetCategoryRecords(categoryName: String): List<Record>


    @Query(
        "SELECT name FROM category INNER JOIN record_category_join ON " +
                "category.name=record_category_join.categoryName WHERE record_category_join.recordId=:recordId"
    )
    fun bgGetRecordCategories(recordId: Long): List<String>

    @Query(
        "SELECT name FROM category INNER JOIN record_category_join ON " +
                "category.name=record_category_join.categoryName WHERE record_category_join.recordId=:recordId"
    )
    fun getRecordCategories(recordId: Long): LiveData<List<String>>


    @Query("SELECT * FROM record_category_join WHERE recordId = :recordId")
    fun getJoinsForRecord(recordId: Long): List<RecordCategoryJoin>
}
