package com.mvgreen.tait.data.db.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import android.os.Parcel
import android.os.Parcelable

@Entity(
    tableName = "record_category_join",
    primaryKeys = ["recordId", "categoryName"],
    foreignKeys = [
        ForeignKey(entity = Record::class, parentColumns = ["id"], childColumns = ["recordId"]),
        ForeignKey(entity = Category::class, parentColumns = ["name"], childColumns = ["categoryName"])]
)
data class RecordCategoryJoin(
    var recordId: Long,
    var categoryName: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(recordId)
        parcel.writeString(categoryName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RecordCategoryJoin> {
        override fun createFromParcel(parcel: Parcel): RecordCategoryJoin {
            return RecordCategoryJoin(parcel)
        }

        override fun newArray(size: Int): Array<RecordCategoryJoin?> {
            return arrayOfNulls(size)
        }
    }
}