package com.mvgreen.tait.data

import android.content.Intent
import android.net.Uri
import android.os.Environment.DIRECTORY_DOCUMENTS
import android.util.Log
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.mvgreen.tait.App
import com.mvgreen.tait.data.db.entities.Category
import com.mvgreen.tait.data.db.entities.Record
import com.mvgreen.tait.data.db.entities.RecordCategoryJoin
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import androidx.core.app.ShareCompat
import com.google.gson.reflect.TypeToken
import com.mvgreen.tait.MainListActivity
import com.obsez.android.lib.filechooser.ChooserDialog


object Repository {

    private val db by lazy { App.DB }

    private const val TAG = "Repository"

    private fun loge(msg: String) = Log.e(TAG, msg)
    private fun logd(msg: String) = Log.d(TAG, msg)

    /** Свойства, связанные с функциями получения LiveData объектов */
    private var _categoryNames: LiveData<List<String>>? = null
    val categoryNames: LiveData<List<String>>
        get() {
            if (_categoryNames == null)
                _categoryNames = db.categoryDao().getCategoryNames()
            return _categoryNames ?: throw AssertionError("Set to null by another thread")
        }

    private val categoryRecords by lazy {
        HashMap<String, LiveData<List<Record>>>()
    }

    /** Функции доступа к БД */
    fun getCategoryRecords(categoryName: String): LiveData<List<Record>> {
        if (categoryRecords.containsKey(categoryName))
            return categoryRecords[categoryName] ?: throw AssertionError("Set to null by another thread")

        return db.joinDao().getCategoryRecords(categoryName)
    }

    fun deleteRecord(record: Record) {
        GlobalScope.launch {
            for (join in db.joinDao().getJoinsForRecord(record.id!!))
                db.joinDao().delete(join)
            db.recordDao().delete(record)
        }
    }

    fun bgGetRecordCategories(record: Record) =
        if (record.exists())
            db.joinDao().bgGetRecordCategories(record.id!!)
        else {
            loge("There's no record with id ${record.id ?: "<NO ID>"}")
            listOf()
        }


    fun getRecordCategories(record: Record): LiveData<List<String>> {
        return db.joinDao().getRecordCategories(record.id!!)
    }

    fun bgUpdateRecordCategories(record: Record, newCategories: List<String>) {
        val joined = db.joinDao().bgGetRecordCategories(record.id!!)
        val names = db.categoryDao().bgGetCategoryNames()

        for (name in names)
            when (name) {
                in newCategories -> if (name !in joined) bgAddRecordCategory(record, name)
                in joined -> if (name !in newCategories) bgRemoveRecordCategory(record, name)
            }

    }

    fun findRecords(query: String) = db.recordDao().findByTag(query)

    private fun bgRemoveRecordCategory(record: Record, categoryName: String) {
        db.joinDao().delete(RecordCategoryJoin(record.id!!, categoryName))
    }

    fun bgAddCategory(category: Category) {
        try {
            db.categoryDao().insert(category)
        } catch (e: Exception) {
        }
    }

    fun bgDeleteCategory(category: Category) {
        val joinDao = db.joinDao()
        val recDao = db.recordDao()
        for (join in db.joinDao().getJoinsForCategory(category.name)) {
            val recordId = join.recordId
            joinDao.delete(join)
            if (joinDao.bgGetRecordCategories(recordId).isNullOrEmpty())
                recDao.delete(recDao.bgGetById(recordId))
        }
        db.categoryDao().delete(category)
    }

    fun bgAddRecord(record: Record, initialCategory: Category) {
        try {
            record.id = db.recordDao().insert(record)
        } catch (e: Exception) {
        }
        try {
            db.joinDao().insert(RecordCategoryJoin(record.id!!, initialCategory.name))
        } catch (e: Exception) {
        }
    }

    fun bgAddRecordCategory(record: Record, categoryName: String) {
        try {
            db.joinDao().insert(RecordCategoryJoin(record.id!!, categoryName))
        } catch (e: Exception) {
            // Запись уже существует, ничего делать не требуется
        }
    }

    private fun String.categoryExists() = db.categoryDao().getCategory(this) != null

    private fun Record.exists() = db.recordDao().hasRecord(id ?: -1) > 0

    private fun Collection<Record>.sort(query: String): List<Record> {
        // В первую очередь выводятся записи с точным совпадением заголовка,
        // затем с точным совпадением в тегах по их порядку к началу списка, остальные - в алфавитном порядке по заголовку
        return this.sortedWith(Comparator { r1: Record, r2: Record ->
            if (query != "") {
                // Проверка по совпадению с заголовком
                if (r1.title == query)
                    return@Comparator -1
                if (r2.title == query)
                    return@Comparator 1

                // Позиция тега в списках
                val tag1Pos = r1.tags.indexOf(query)
                val tag2Pos = r2.tags.indexOf(query)

                // Если тега нет ни в одном, или они находятся в равном приоритете, сравнение заголовков
                if (tag1Pos == tag2Pos)
                    return@Comparator r1.title.compareTo(r2.title)

                // Если тега нет в одном из списков
                if (tag1Pos == -1)
                    return@Comparator 1
                if (tag2Pos == -1)
                    return@Comparator -1

                // Если есть, выше тот, у которого тег раньше
                return@Comparator tag1Pos - tag2Pos
            }
            return@Comparator r1.title.compareTo(r2.title)

        })
    }

    fun isValidCategoryName(catName: String): Boolean {
        if (catName.isEmpty())
            return false
/*        for (c in catName) {
            if (!c.isLetterOrDigit() && c != ' ' && c != '-')
                return false
        }*/
        return true
    }

    fun checkHasCategory(catName: String): LiveData<Boolean> {
        val hasCat = MutableLiveData<Boolean>()
        GlobalScope.launch {
            hasCat.postValue(catName.categoryExists())
        }
        return hasCat

    }

    fun mergeCategories(listToReplace: MutableList<String>, newName: String) {
        GlobalScope.launch {
            if (newName in listToReplace)
                listToReplace.remove(newName)
            else
                bgAddCategory(Category(newName))

            for (categoryName in listToReplace) {
                db.joinDao().bgGetCategoryRecords(categoryName).let { list ->
                    for (r in list) {
                        bgRemoveRecordCategory(r, categoryName)
                        bgAddRecordCategory(r, newName)
                    }
                }
                bgDeleteCategory(Category(categoryName))
            }
        }
    }

    fun bgCreateRecordFile(activity: MainListActivity): Boolean {
        if (activity.vm.recordToShare == null)
            return false

        val record = activity.vm.recordToShare!!

        val dir = File(activity.getExternalFilesDir(DIRECTORY_DOCUMENTS), "tait_notes")
        if (!dir.exists() || !dir.isDirectory) {
            if (!dir.mkdir())
                return false
        }

        val output = File(dir, "${record.title}${Calendar.getInstance().time.time}.tait")
        if (!output.createNewFile())
            return false

        output.writeText(Gson().toJson(listOf(record)))

        val fileUri: Uri = try {
            FileProvider.getUriForFile(
                activity,
                "com.mvgreen.tait.fileprovider",
                output
            )
        } catch (e: IllegalArgumentException) {
            Log.e(
                "File Selector",
                "The selected file can't be shared: $output"
            )
            null
        } ?: return false

        val shareIntent = ShareCompat.IntentBuilder.from(activity)
            .setStream(fileUri)
            .intent
        // Provide read access
        shareIntent.data = fileUri
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        return if (shareIntent.resolveActivity(activity.packageManager) != null) {
            activity.startActivity(shareIntent)
            true
        } else
            false
    }

    fun bgCreateCategoryFile(activity: MainListActivity, categoryName: String?): Boolean {
        if (categoryName == null)
            return false
        val dir = File(activity.getExternalFilesDir(DIRECTORY_DOCUMENTS), "tait_notes")
        if (!dir.exists() || !dir.isDirectory) {
            if (!dir.mkdir())
                return false

        }

        val output = File(dir, "$categoryName${Calendar.getInstance().time.time}.tait")
        if (!output.createNewFile())
            return false

        val list = db.joinDao().bgGetCategoryRecords(categoryName)
        output.writeText(Gson().toJson(list))

        val fileUri: Uri = try {
            FileProvider.getUriForFile(
                activity,
                "com.mvgreen.tait.fileprovider",
                output
            )
        } catch (e: IllegalArgumentException) {
            Log.e(
                "File Selector",
                "The selected file can't be shared: $output"
            )
            null
        } ?: return false

        val shareIntent = ShareCompat.IntentBuilder.from(activity)
            .setStream(fileUri)
            .intent
        // Provide read access
        shareIntent.data = fileUri
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        return if (shareIntent.resolveActivity(activity.packageManager) != null) {
            activity.startActivity(shareIntent)
            true
        } else
            false
    }

    fun importRecords(activity: MainListActivity) {
        ChooserDialog(activity)
            .withFilter { it.isDirectory || it.extension == "tait" }
            .withStartFile("/")
            .withChosenListener { path, pathFile ->
                val records: List<Record> = Gson().fromJson<List<Record>>(pathFile.reader(), object : TypeToken<List<Record>>() {}.type)
                activity.onRecordImported(records)
            }
            .build()
            .show()
    }

}