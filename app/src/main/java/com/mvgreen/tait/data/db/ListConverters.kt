package com.mvgreen.tait.data.db

import androidx.room.TypeConverter

class ListConverters {
    @TypeConverter
    fun fromString(s: String?) = s?.split(",", ", ") ?: mutableListOf()

    @TypeConverter
    fun fromList(list: List<String>) = list.joinToString()
}