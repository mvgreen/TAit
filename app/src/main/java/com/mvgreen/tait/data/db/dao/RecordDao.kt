package com.mvgreen.tait.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mvgreen.tait.data.db.entities.Record

@Dao
interface RecordDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(record: Record): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(record: Record)

    @Delete
    fun delete(record: Record)

    @Query("SELECT * FROM record WHERE id == :id")
    fun getById(id: Long): LiveData<Record>

    @Query("SELECT * FROM record WHERE id == :id")
    fun bgGetById(id: Long): Record

    @Query("SELECT * FROM record WHERE title LIKE '%' || :s || '%' OR tags LIKE '%' || :s || '%'")
    fun findByTag(s: String): LiveData<List<Record>>

    @Query("SELECT last_insert_rowid()")
    fun getLastID(): Long

    @Query("SELECT * FROM record WHERE id = :recordId")
    fun hasRecord(recordId: Long): Long
}