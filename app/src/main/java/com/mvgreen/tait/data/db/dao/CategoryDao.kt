package com.mvgreen.tait.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mvgreen.tait.data.db.entities.Category

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(category: Category)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(category: Category)

    @Delete
    fun delete(category: Category)

    @Query("SELECT name FROM category")
    fun getCategoryNames(): LiveData<List<String>>

    @Query("SELECT name FROM category")
    fun bgGetCategoryNames(): List<String>

    @Query("SELECT * FROM category WHERE name = :name")
    fun getCategory(name: String): Category?

    @Query("SELECT * FROM category WHERE name = :categoryName")
    fun hasCategory(categoryName: String): Long
}
