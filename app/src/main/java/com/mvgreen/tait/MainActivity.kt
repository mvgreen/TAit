package com.mvgreen.tait

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.mvgreen.tait.data.db.entities.Record
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import com.mvgreen.tait.data.Repository as Repo

class MainActivity : AppCompatActivity() {

    var r2: Record? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*GlobalScope.launch {
            var r1 = Record(title = "Record1")
            r2 = Record(title = "Record2")
            var cat = Category("Category1")

            Repo.bgAddCategory(cat)
            Repo.bgAddRecord(r1, cat)
            Repo.bgAddRecord(r2!!, cat)

            ld = Repo.getCategoryRecords(cat.name).apply {
                observeForever{
                    it?.let {
                        Log.d("TEST", ld!!.value.toString())
                    }
                }
            }
            Repo.deleteRecord(r1)
        }*/
    }

    fun onClick(view: View) {
        GlobalScope.launch {
            Repo.deleteRecord(r2!!)
        }
    }
}
