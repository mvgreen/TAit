package com.mvgreen.tait

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mvgreen.tait.data.db.entities.Record
import kotlinx.android.synthetic.main.activity_database.*
import kotlinx.android.synthetic.main.content_edit.*
import com.google.android.material.snackbar.Snackbar as Snack


class EditActivity : AppCompatActivity() {

    var exists: Boolean = false
    var saved: Boolean = false
    lateinit var record: Record

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        setSupportActionBar(toolbar)
        saved = false
        fab.setOnClickListener {
                if (record_title.text == null ||
                    record_title.text.toString().isEmpty() ||
                    record_content.text == null ||
                    record_content.text.toString().isEmpty()
                ) {
                    Snack.make(it, "Заполните поля заголовка и содержания!", Snack.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
                record.title = (record_title.text.toString())
                record.content = (record_content.text.toString())
                record.tags = tags.text.toString().split(",").map { it.trim() }
                saved = true
                onBackPressed()
        }


        // Заполнение полей
        if (Intent.ACTION_SEND == intent.action && intent.type != null) {
            if ("text/plain" == intent.type) {
                val sharedText = intent.getStringExtra(Intent.EXTRA_TEXT)
                record = Record(content = sharedText)
                exists = false
            }
        } else if (intent.hasExtra("RECORD_TO_EDIT")) {
            record = intent.getParcelableExtra("RECORD_TO_EDIT")
            exists = true
        } else if (Intent.ACTION_SEND == intent.action
            && intent.type != null && intent.type == "text/plain"
        ) {
            record = Record(content = intent.getStringExtra(Intent.EXTRA_TEXT))
            exists = false
        } else {
            record = Record()
            exists = false
        }

        record_title.setText(record.title)
        tags.setText(record.tags.toString())
        record_content.setText(record.content)
    }

    fun handleSendText(intent: Intent) {
        val sharedText = intent.getStringExtra(Intent.EXTRA_TEXT)
        if (sharedText != null) {
            // Update UI to reflect text being shared
        }
    }

     override fun onBackPressed() {
        if (saved) {
            if (!exists) {
                try {
                    // Сохранить в бд
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                    Toast.makeText(this, "При добавлении записи произошла ошибка!", Toast.LENGTH_LONG).show()
                }
            } else {
                try {
                    // Обновить в бд
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                    Toast.makeText(this, "При сохранении изменений произошла ошибка!", Toast.LENGTH_SHORT).show()
                }
            }
            super.onBackPressed()
        } else
            AlertDialog.Builder(this)
                .setMessage("Вы уверены, что хотите отменить изменения и вернуться?")
                .setTitle("Вы покидаете редактор без сохранения!")
                .setPositiveButton("Да") { _, _ ->
                    super@EditActivity.onBackPressed()
                }
                .setNegativeButton("Нет") { dialog, _ ->
                    dialog.cancel()
                }
                .show()
    }
}
