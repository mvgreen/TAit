package com.mvgreen.tait

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.db_fragment.*

class ContentFragment : Fragment() {

    lateinit var vm: DbViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.db_fragment, container, false)

    override fun onResume() {
        super.onResume()

        vm = (activity as MainListActivity).vm
        val titles = vm.tabTitles

        // Настройка табов
        view_pager.adapter = SectionsPagerAdapter(this.childFragmentManager, vm)
        with(category_tabs) {
            setupWithViewPager(view_pager)
            tabMode = TabLayout.MODE_SCROLLABLE
        }

        titles.observe(this, Observer {
            vm.openedTab?.let { tab ->
                view_pager.currentItem = tab
                vm.openedTab = null
            }
            (view_pager.adapter as SectionsPagerAdapter).updateTitles(it)
        })
    }

    override fun onPause() {
        super.onPause()
        vm.openedTab = category_tabs.selectedTabPosition
    }

    override fun onDestroy() {
        super.onDestroy()
        vm.openedTab = null
    }
}