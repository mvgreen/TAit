package com.mvgreen.tait

import android.app.Application
import com.mvgreen.tait.data.db.RecordDatabase
import com.mvgreen.tait.data.db.entities.Category
import com.mvgreen.tait.data.db.entities.Record
import com.mvgreen.tait.data.db.entities.RecordCategoryJoin
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        _instance = this
        _db = RecordDatabase.getDB(this)

        var r1: Long
        var r2: Long

        GlobalScope.launch {
            try {
                with(DB.categoryDao()) {
                    insert(Category("Физика"))
                    insert(Category("Матан"))
                }
                with(DB.recordDao()) {
                    r1 = insert(Record(title = "record1", content = "the first record"))
                    r2 = insert(Record(title = "record2", content = "the second record"))
                }
                with(DB.joinDao()) {
                    insert(RecordCategoryJoin(r1, "Физика"))
                    insert(RecordCategoryJoin(r2, "Физика"))
                    insert(RecordCategoryJoin(r2, "Матан"))
                }
            } catch (e: Exception) {}
        }
    }

    companion object {
        private lateinit var _instance: App
        val INSTANCE: App
            get() = _instance

        private lateinit var _db: RecordDatabase
        val DB: RecordDatabase
            get() = _db
    }
}