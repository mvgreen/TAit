package com.mvgreen.tait

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.navigation.NavigationView
import com.mvgreen.tait.R.id.db_mode
import com.mvgreen.tait.R.id.navigation_view

class BottomNavigationDrawerFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_bottomsheet, container, false)
        v.findViewById<NavigationView>(navigation_view).setNavigationItemSelectedListener { menuItem ->
            ViewModelProviders.of(this.activity as AppCompatActivity).get(DbViewModel::class.java)
                .activityMode =
                    if (menuItem.itemId == db_mode) DbViewModel.Companion.ActivityMode.ACTIVITY_DATABASE
                    else DbViewModel.Companion.ActivityMode.ACTIVITY_SEARCH
            activity?.recreate()
            this@BottomNavigationDrawerFragment.dismiss()
            true
        }
        return v
    }


}
