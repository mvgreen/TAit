package com.mvgreen.tait

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import com.mvgreen.tait.data.db.entities.Record
import kotlinx.android.synthetic.main.db_card.view.*

class DbListAdapter(val context: Context, var items: List<Record> = listOf()) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class VHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.tv_title
        val content: TextView = itemView.tv_content
        val popupButton: ImageButton = itemView.popup_button
    }

    override fun onBindViewHolder(vh: RecyclerView.ViewHolder, position: Int) {
        (vh as VHolder).title.text = items[position].title
        vh.content.text = items[position].content

        vh.itemView.setOnClickListener {
            context.startActivity(
                (Intent(context, EditActivity::class.java)).apply {
                    putExtra("RECORD", items[position])
                })
        }

        vh.popupButton.setOnClickListener {
            with (PopupMenu(context, it)) {
                inflate(R.menu.db_card_actions)
                setOnMenuItemClickListener { item ->
                    when(item.itemId) {
                        R.id.share_card ->
                            (context as MainListActivity).shareRecord(items[position])
                        R.id.other_categories_card -> (context as MainListActivity).showRecordCategories(items[position])
                        R.id.delete_card -> (context as MainListActivity).showDeleteDialog(items[position])
                    }
                    true
                }
                show()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.db_card, parent, false)
        return VHolder(v)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun update(list: List<Record>) {
        items = list
        notifyDataSetChanged()
    }
}
