package com.mvgreen.tait

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mvgreen.tait.data.Repository
import com.mvgreen.tait.data.db.entities.Record

class DbViewModel : ViewModel() {
    fun clear() {
        activityMode = ActivityMode.ACTIVITY_DATABASE
        searchQueries = MutableLiveData<List<String>>().apply { value = listOf() }
        searchOpened = false
        openedTab = null
        loadingDialog = null
        recordToShare = null
    }

    var activityMode = ActivityMode.ACTIVITY_DATABASE
    private var searchQueries = MutableLiveData<List<String>>().apply { value = listOf() }

    val tabTitles
        get() = if (activityMode == ActivityMode.ACTIVITY_DATABASE) Repository.categoryNames else searchQueries
    var searchOpened = false
    var openedTab: Int? = null
    var loadingDialog: AlertDialog? = null
    var recordToShare: Record? = null

    companion object {
        enum class ActivityMode {
            ACTIVITY_SEARCH, ACTIVITY_DATABASE
        }
    }
}